import pandas as pd
import numpy as np 
import argparse


SUBJECTS = ['computer', 'engineering', 'astronomy', 'physics',
            'information theory', 'aeronautic', 'naval', 'chemical',
            'ocean engineering', 'science', 'statistics', 'mathematics',
            'computational', 'technology', 'astronautics']
OUT_FIELDS = ['Journal title', 'Journal URL', 'Journal EISSN (online version)',
              'Review process', 'Journal license',
              'Does this journal allow unrestricted reuse in compliance with BOAI?',
              'Author holds copyright without restrictions',
              'Author holds publishing rights without restrictions',
              'DOAJ Seal']


parser = argparse.ArgumentParser(
    description='DOAJ CSV file parser')
parser.add_argument("CSV_PATH",
    help="Path of the input CSV file")

args = parser.parse_args()
df = pd.read_csv(args.CSV_PATH)
print(df.columns.values)

# Exclude all the journals with either submission fees or processing charges
mask_apc = df['Journal article processing charges (APCs)'].values == 'No'
mask_fee = np.logical_or(np.isnan(df['Submission fee amount'].values),
                         df['Submission fee amount'].values == 0)
df = df[np.logical_and(mask_apc, mask_fee)]

# Only English journals
langs = df['Full text language'].values
mask = [isinstance(l, str) and 'english' in l.lower() for l in langs]
df = df[mask]

# Filter by subject
mask = []
for subject in df['Subjects'].values:
    if not isinstance(subject, str):
        mask.append(False)
        continue
    mask.append(np.any([word.lower() in subject.lower() for word in SUBJECTS]))
df = df[mask]

# Get just the selected columns
df = df[OUT_FIELDS]
df.to_csv(args.CSV_PATH + '.parsed.csv', sep=',', encoding='utf-8')
